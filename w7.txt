Introduction
This week our focus will be in the theory, specifically the most common software design patters.

 

Objectives:

Be able to compare the differences and similitudes of common patterns.
Be able to explain how a pattern works, when should be used, what use case it has.
Understand and explain what patterns are used in Node.js and Nestjs.
Know the WH questions regarding antipatterns.
Software design patterns
https://refactoring.guru/design-patterns/what-is-pattern

https://github.com/kamranahmedse/design-patterns-for-humans

Factory Method
https://refactoring.guru/design-patterns/factory-method

Abstract Factory
https://refactoring.guru/design-patterns/abstract-factory

Builder
https://refactoring.guru/design-patterns/builder

Prototype
https://refactoring.guru/design-patterns/prototype

Singleton
https://refactoring.guru/design-patterns/singleton

Adapter
https://refactoring.guru/design-patterns/adapter

Proxy
https://refactoring.guru/design-patterns/proxy

Decorator
https://refactoring.guru/design-patterns/decorator

Facade
https://refactoring.guru/design-patterns/facade

Chain of responsibility
https://refactoring.guru/design-patterns/chain-of-responsibility

Strategy
https://refactoring.guru/design-patterns/strategy

Template Method
https://refactoring.guru/design-patterns/template-method

Dependency Injection
https://www.freecodecamp.org/news/a-quick-intro-to-dependency-injection-what-it-is-and-when-to-use-it-7578c84fa88f/

Antipatterns
https://sourcemaking.com/antipatterns

Homework
With your new knowledge on Design Patterns, answer: (The questions should be answered on your readme.md file)

Which patterns does Nest.JS use?
Why?
How are they implemented?
Which patterns can be used on your application?
How those patterns could be implemented?
Explain how to implement the Dependency Injection pattern in Typescript.
Include a code example.
Explain in your own words, what an anti-pattern is.
 

Creating a branch off your latest valid version of news API, do the following:

Implement at least 2 design patterns in your API.
Important: The ones implemented by Nest.js won't be taken into account.
Document on readme.md: Why did you use them?
Remove at least 2 anti-patterns that you can found on your API side.
Document on readme.md: Why did you think it is an anti-pattern?
Important: If you conclude no antipatterns are used in your code, full score for this AC will be given ONLY if reviewer does not find any as well.
Summary

At the end of the readme.md bullet list.
Which patterns were used.
Which antipatterns were removed.
