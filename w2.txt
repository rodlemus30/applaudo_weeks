Introduction
The past week we learned about JavaScript, this week, we move forward with TypeScript. The goal will be to understand what things from JavaScript are improved. We will be using TypeScript from now on as much as possible.

TypeScript Setup
https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html

https://www.typescriptlang.org/docs/handbook/tsconfig-json.html

TypeScript Introduction

https://www.youtube.com/watch?v=NR0nkEzr0lY

https://www.youtube.com/watch?v=ieqvAF3fAlA

https://www.youtube.com/watch?v=tmVh58O_7uc

https://www.youtube.com/watch?v=StHYJMBsS5g

https://www.youtube.com/watch?v=O9pEVDzMYac3 


Types
  https://www.youtube.com/watch?v=-YX71UnaAbA

  https://www.youtube.com/watch?v=3kre07A8F4I

  https://www.youtube.com/watch?v=p6Iah50FZFQ

  https://www.youtube.com/watch?v=kPWycV8Pzvs

  https://www.youtube.com/watch?v=P3X9b-_aEZg

  https://www.youtube.com/watch?v=FzFqrjoUwsI

  https://www.youtube.com/watch?v=UMhv4hFfPok

  https://www.youtube.com/watch?v=XaRoqqCr-CQ

  https://www.youtube.com/watch?v=8mB60de6ld8

  https://www.youtube.com/watch?v=Ps-i1cg1wMM4

Classes
  https://www.youtube.com/watch?v=pjH6-IczZcs

  https://www.youtube.com/watch?v=QYPOt-azj1Q

  https://www.youtube.com/watch?v=ozwj2EkN434

  https://www.youtube.com/watch?v=l3XJM6rB8Mo

  https://www.youtube.com/watch?v=Tak9kOMGl2o

  https://www.youtube.com/watch?v=2tyxUW6Uz6A

  https://www.youtube.com/watch?v=ldMIQg5U89Y

  https://www.youtube.com/watch?v=A51EcFlFzs0

  https://www.youtube.com/watch?v=lhALGF3dzks

  https://www.youtube.com/watch?v=OD--eixemfA

  https://www.youtube.com/watch?v=bv81CwCnYok5

Object Literals
  https://www.youtube.com/watch?v=gvgUBfYo9Mc

  https://www.youtube.com/watch?v=PWhREbRlfeQ

  https://www.youtube.com/watch?v=_-bSzvBO6O4

  https://www.youtube.com/watch?v=QiDkx_kqrmo

  https://www.youtube.com/watch?v=hPXvEgtQ2WQ

  https://www.youtube.com/watch?v=wnZkQ2weZIg

  https://www.youtube.com/watch?v=vl-IC23WRU8

  https://www.youtube.com/watch?v=KTYBHfgz5jU6 

Generics
  https://www.youtube.com/watch?v=oNKMvdoG4uU

  https://www.youtube.com/watch?v=FfkrGZoaqKA

  https://www.youtube.com/watch?v=M-Q2REV2Y_8 

  https://www.youtube.com/watch?v=kaN7QkuEnfU

  https://www.youtube.com/watch?v=cGyGor65JmA7

Iterators
  https://www.youtube.com/watch?v=EdM7ch3kn6k

  https://www.youtube.com/watch?v=mLVvhvsGlpc

  https://www.youtube.com/watch?v=u6XO2x1zz8Q

  https://www.youtube.com/watch?v=5GsDcgk1ff4

  https://www.youtube.com/watch?v=cyeqDTm-VUI

  https://www.youtube.com/watch?v=cyeqDTm-VUI8

Object Oriented Programming
 https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects

 https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS

Functional Programming
https://flaviocopes.com/javascript-functional-programming/

https://maryrosecook.com/blog/post/a-practical-introduction-to-functional-programming10

Homework
Using types and taking advantage of the knowledge gained during the week, create:

An application that receives two parameters.
A file name
An option flag.
The application should be able to encode a file using run length encoding.
reference: https://en.wikipedia.org/wiki/Run-length_encoding
The application should be able to read a file encoded with run length encoding and decode it.
Input files will only contain text when decoded.
The option flag determines whether to encode or decode the given file.
The application can override the file, or create another one. This is left optional to the developer.
 

Extra credit: Add tslint/eslint to the project.

