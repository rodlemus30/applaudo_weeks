Introduction
Last week we were introduced to Nest.js, this week and moving forward we will use this framework to build our APIs. On week 6 we focus on data input, we make sure only the clients we need can CRUD our data (Authentication), we know how to persist our data (Databases) and parse our data properly (Serialization). And of course, we explore a little bit about security.

Objectives:
Understand the WH questions around JWT.
Provide an educated opinion and alternatives and use cases for JWT.
Understand how to use a database with Nest.js.
Understand the WH questions around TypeORM.
Understand the WH questions around Mongooose.
Understand how and why data can be serialized and validated on Nest.js.
Know what security features to implement/override with Nest.js, like CORS.
Authentication
https://docs.nestjs.com/techniques/authentication

https://jwt.io/introduction/

https://auth0.com/learn/json-web-tokens/?_ga=2.213022292.1759381597.1607392411-1641020.1607392411

https://dzone.com/articles/cookies-vs-tokens-the-definitive-guide

https://medium.com/@sherryhsu/session-vs-token-based-authentication-11a6c5ac45e4

[Overview] http://www.passportjs.org/docs/

http://www.passportjs.org/docs/authenticate/

Database
https://docs.nestjs.com/techniques/database


https://www.tutorialspoint.com/typeorm/typeorm_introduction.htm

https://www.tutorialspoint.com/typeorm/typeorm_entity.htm

https://www.tutorialspoint.com/typeorm/typeorm_relations.htm

https://www.tutorialspoint.com/typeorm/typeorm_query_builder.htm

https://github.com/typeorm/typeorm

https://docs.nestjs.com/techniques/mongodb

https://www.freecodecamp.org/news/introduction-to-mongoose-for-mongodb-d2a7aa593c57/

https://www.voidcanvas.com/mongoose-vs-mongodb-native/


Data Validation
https://docs.nestjs.com/techniques/validation

 

Serialization
https://docs.nestjs.com/techniques/serialization

Security

https://docs.nestjs.com/techniques/security

https://web.dev/cross-origin-resource-sharing/

https://auth0.com/blog/cors-tutorial-a-guide-to-cross-origin-resource-sharing/


Homework
We will use the code base from the same API as last week, our news API.

 

General Instructions:

First create a branch off the latest valid commit from your last project.
Second, add new commits (this week's work) on that new branch.
When you have your project complete for week 6, create a merge request towards your master (last week) branch.
DO NOT MERGE THE MERGE REQUEST
Provide the same link as homework, and also a link to the merge request, so we can see what was added.
 

Acceptance Criteria:

Add one new API for news into your project, you now must have at least 3 news sources.
You can use but not limited to: https://rapidapi.com/blog/rapidapi-featured-news-apis/
Your api must accept user creation thru the use of REST endpoints.
Create a user
Update a user
Delete a user
Login/Logout endpoints must be provided.
Only logged users must be able to view and save articles to their account thru the use of REST endpoints.
You should only need user id and article URL.
Articles saved can only be their URL or a payload describing their content.
Users must be able to list their saved news.
JWT authentication strategy must be used.
 

 

As an extra point, you may be able to "send" articles to other accounts as "recommendations", in order to view them there should be an endpoint to query them.

