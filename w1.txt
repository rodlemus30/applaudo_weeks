Environment Setup.
For developing on Node.js you will only need the Node.js runtime and a package manager. Usually "npm" comes bundled with Node.js in most Linux package managers. Optionally you may use nvm (Node Version Manager) to install and manage different versions of node and you may use Yarn as your package manager if you prefer. A linter and a code formatter are nice to haves, also.
Above is a list of resources you need to be familiar with for the rest of the program.  

 

https://github.com/yarnpkg/yarn

https://nodejs.org/en/download/

https://github.com/nvm-sh/nvm

https://eslint.org/docs/user-guide/getting-started

https://prettier.io/docs/en/index.html

https://prettier.io/docs/en/integrating-with-linters.html

https://github.com/airbnb/javascript

Version control with git.
It is expected that all projects made within this trainee program are managed with git.

Using good commit messages, understanding how git works, how to use it and how to commit are the goals of this section.

In order to work in teams and have a maintainable repository with minimal conflicts, git strategies are used. Gitflow will be reviewed, which is a nice strategy to follow.

 

http://rogerdudler.github.io/git-guide/

https://guides.github.com/introduction/git-handbook/

https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

https://chris.beams.io/posts/git-commit/

https://developer.ibm.com/technologies/web-development/tutorials/d-learn-workings-git/

Node package management.
Node has its own way of managing dependencies.

This section will show what they are and how they work.

 

https://www.sitepoint.com/npm-guide/

https://classic.yarnpkg.com/en/docs/getting-started

https://docs.npmjs.com/cli/v6/configuring-npm/package-json

https://www.toptal.com/javascript/a-guide-to-npm-the-node-package-manager

Data Types, Operators and Data Structures.
https://eloquentjavascript.net/01_values.html

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures

https://eloquentjavascript.net/04_data.html

Conditionals and Branching.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/switch

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator

Loops and map, filter, reduce.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration

https://medium.com/poka-techblog/simplify-your-javascript-use-map-reduce-and-filter-bd02c593cc2d

https://eloquentjavascript.net/02_program_structure.html

JavaScript Loops - Code This, Not That

Functions.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions

https://dmitripavlutin.com/6-ways-to-declare-javascript-functions/

https://eloquentjavascript.net/03_functions.html

Asynchronous programming (Callbacks and Promises).
https://eloquentjavascript.net/11_async.html

https://closebrace.com/tutorials/2017-01-17/understanding-javascript-callbacks

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises

What the heck is the event loop anyway? | Philip Roberts | JSConf EU

https://www.youtube.com/watch?v=bvgKIpE3PLc

https://www.youtube.com/watch?v=eyaAQNuu2uw

https://www.youtube.com/watch?v=Zw22T8KfqZ8

https://www.youtube.com/watch?v=jJ96mmtsS9k

https://www.youtube.com/watch?v=c30R00eWGPc

https://www.youtube.com/watch?v=ehFDxQvA1yA

https://www.youtube.com/watch?v=GgoazcU5-_g

https://www.youtube.com/watch?v=sdE4dlfhm20

https://www.youtube.com/watch?v=ePwi5nsIerI

https://www.youtube.com/watch?v=jQWvwf7nOB4

https://www.youtube.com/watch?v=msCokR8jJr0

https://www.youtube.com/watch?v=qHryMjrLZTE

Async/Await.
The Async Await Episode I Promised

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function

Module system (CommonJS and ES Modules).
https://adrianmejia.com/getting-started-with-node-js-modules-require-exports-imports-npm-and-beyond/

https://eloquentjavascript.net/10_modules.html

https://www.freecodecamp.org/news/javascript-modules-a-beginner-s-guide-783f7d7a5fcc/

https://nodejs.dev/learn/expose-functionality-from-a-nodejs-file-using-exports

Command Line: Accepting and parsing arguments.
Node.js offers a way to handle command line arguments. Parsing said arguments can be difficult for more complex scenarios (such as accepting command line options) in which case using a library such as yargs may be helpful.

https://nodejs.dev/learn/nodejs-accept-arguments-from-the-command-line

http://yargs.js.org/docs/

File system using path and fs.
https://nodejs.dev/learn/nodejs-file-paths

https://nodejs.dev/learn/reading-files-with-nodejs

https://nodejs.dev/learn/the-nodejs-fs-module

https://nodejs.dev/learn/the-nodejs-path-module

Regular Expressions.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions

https://flaviocopes.com/javascript-regular-expressions/#introduction-to-regular-expressions

Strict mode.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode

Homework.
Sed is a Unix utility used to parse and transform text. Reference: https://archive.flossmanuals.net/command-line/sed.html

 

For this assignment a simple and naive version of sed will be created.

This requirements will be:

 

Only the substitution command is required.
Create a command line utility that accepts options and arguments.
The first argument will be the command and the second one will be the file.
Check if the file specified exists.
Check if the substitution command is valid.
Implement the -n option, which prevents a line from being printed unless specified by ‘p’.
Implement the -i [extension] which tells sed to edit the file instead of printing to the standard output.
A copy of the original should be saved in [file_name].[extension]
Multiple substitution commands should be accepted with the -e option, with one command per -e appearance.
When the -n option appears, the line should not be printed to the standard output unless specified.
The -f [script-file] option should expect a file which consists of several lines containing one command each.
Implement the p flag which tells that the line should be printed.
Implement the g flag which tells that all occurrences of the search should be replaced on the line. By default only the first occurrence is replaced.
 

NOTE: You can use this online tool, to get a feel of how sed works if you are not using a unix machine: https://sed.js.org/

